<?php
//Definimos la codificación de la cabecera.
header('Content-Type: text/html; charset=utf-8');

//Importamos el archivo con las validaciones.
require_once 'funciones/validaciones.php';

//Guarda los valores de los campos en variables, siempre y cuando se haya enviado el formulario, sino se guardará null.
$usuario = isset($_POST['usuario']) ? $_POST['usuario'] : null;

//Este array guardará los errores de validación que surjan.
$errores = array();

//Pregunta si está llegando una petición por POST, lo que significa que el usuario envió el formulario.
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
   //Valida que el campo nombre no esté vacío.
   if (!comprobar_nombre_usuario($usuario)) {
      $errores[] = 'El campo nombre es incorrecto.';
   }
   //Verifica si ha encontrado errores y de no haber redirige a la página con el mensaje de que pasó la validación.
   if(!$errores){
      header('Location: validado.php');
      exit;
   }
}
?>
<?php 

$password = 'jesus';
$salt = '$bgr$/';

/**echo $password = sha1(md5($salt , $password));*/

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/semujer.jpg">

    <title>SE-MUJER</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">
	
	<?php if ($errores): ?>
       <ul style="color: #f00;">
          <?php foreach ($errores as $error): ?>
             <li> <?php echo $error ?> </li>
          <?php endforeach; ?>
       </ul>
    <?php endif; ?>

      <form class="form-signin" action="controlador.php" action="login.php" method="POST" >
        <center><h2 class="form-signin-heading">SE-MUJER</h2></center>
        <br>
          <label for="input" class="sr-only">Usuario</label>
        
          <input name="usuario" type="text" autofocus required class="form-control" id="inputUsers" placeholder="Usuario" value ="<?php echo $usuario ?>"></br>
        
      <label for="inputPassword" class="sr-only">Password</label>
        
          <input type="password" name="clave" id="inputPassword" class="form-control" placeholder="Contraseña" required>
       
        <br><button class="btn btn-lg btn-primary btn-block" type="submit">INGRESAR</button></br>
      
      </form>
    

    </div> <!-- /container -->

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
