<?php
 function comprobar_nombre_usuario($usuario){ 
   //compruebo que el tamaño del string sea valido. 
   if (strlen($usuario)<3 || strlen($usuario)>20){ 
      echo $usuario . " no es valido<br>"; 
      return false; 
   } 

   //compruebo que los caracteres sean los permitidos 
   $permitidos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_"; 
   for ($i=0; $i<strlen($usuario); $i++){ 
      if (strpos($permitidos, substr($usuario,$i,1))===false){ 
         echo $usuario . " no es valido<br>"; 
         return false; 
      } 
   } 
   echo $usuario. " es valido<br>"; 
  	
   return true; 
}

?>
